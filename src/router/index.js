import Vue from 'vue';
import Router from 'vue-router';

// Main Components and Layout
import Main from '../components/Main.vue';
import Login from '../components/auth/login.vue';

// Team
import home from '../components/teams/home.vue'
import create from '../components/teams/create.vue';
import edit from '../components/teams/edit.vue';

// Players
import homePlayers from '../components/players/home.vue'; 
import createPlayers from '../components/players/create.vue';
import editPlayers from '../components/players/edit.vue';

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
       {
        path: '/',
        name: 'Main',
        component: Main,
        redirect:'/teams-home'
       },
       {
        path: '/login',
        name: 'Login',
        component: Login
       },
       {
        path: '/teams-home',
        name: 'home',
        component: home
       },
       {
        path: '/teams-create',
        name: 'create',
        component: create
       },
       {
        path: '/teams-edit/:id',
        name: 'edit',
        component: edit
       },
       {
           path:'/players-home',
           name:'homePlayers',
           component: homePlayers
       },
       {
           path:'/players-create',
           name: 'createPlayers',
           component:createPlayers
       },
       {
            path:'/players-edit/:id',
            name: 'editPlayers',
            component:editPlayers
        }

    ]
})